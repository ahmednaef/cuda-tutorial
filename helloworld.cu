#include<stdio.h>

__global__ void hello()
{
	/*
		Thread block is executed in a different order every time.
	*/
	printf("Hello World From GPU - Block ID %d and Thread ID %d\n", blockIdx.x, threadIdx.x);
}

int main()
{
	int number_of_threads = 1000;
	int number_of_blocks=2;
	hello<<<number_of_blocks, number_of_threads>>>();
	cudaDeviceSynchronize();
	return 0;
}