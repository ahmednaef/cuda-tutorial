#include<stdio.h>
#define N 400
#define BLOCK_DIM 20

#define X 20
#define Y 20


__global__ void matrixAddition (int *a, int *b, int *c) {
	
	/*
		for a one-dimensional Grid and a two-dimensional block of size (Dx, Dy),
		the thread ID is x+y * blockDim.x 
		
		N <= the maximum number of threads per block (N <=1024)
		Maximum value of X & Y is 32
	*/
	
	int index = threadIdx.x + threadIdx.y * blockDim.x;
	if (index < N) {
		c[index] = a[index] + b[index];
	}
}

void dispay(int a[][Y])
{
for (int i=0;i<X;++i)
	{  for (int j=0;j<Y;++j)
		printf("%d\t",a[i][j]);
		printf("\n");
	}
}

void set(int a[][Y])
{
for (int i=0;i<X;++i)
  for (int j=0;j<Y;++j)
    a[i][j]=rand() %20;
}

int main() {
	//declare all variables
	 int a[X][Y], b[X][Y], c[X][Y];
	 int *dev_a, *dev_b, *dev_c;
	 //determine the number of bytes
	 int size = X * Y * sizeof(int);
	 // initialize host 2D arrays a and b
	set(a);
	set(b);
	// allocate device memory
	cudaMalloc((void**)&dev_a, size);
	cudaMalloc((void**)&dev_b, size);
	cudaMalloc((void**)&dev_c, size);
	//transfer data from host memory to device memory 
	cudaMemcpy(dev_a, a, size, cudaMemcpyHostToDevice);
	cudaMemcpy(dev_b, b, size, cudaMemcpyHostToDevice);
	//launch the kernel 
	dim3 dimBlock(X, Y);
	matrixAddition<<<1,dimBlock>>>(dev_a,dev_b,dev_c);

	dispay(a);
	printf("\n\n");
	dispay(b);
	//transfer result back to host memory from device memory
	cudaMemcpy(c, dev_c, size, cudaMemcpyDeviceToHost);
	printf("\n\n");
	dispay(c);
	//deallocate device memory
	cudaFree(dev_a); cudaFree(dev_b); cudaFree(dev_c);
}
