#include<stdio.h>
#define N 10  // vector size
#define T 10 // max threads per block
__global__ void shiftRight (int *a){
	int tid = threadIdx.x + blockDim.x*blockIdx.x;
	if(tid<N)
	{
		int tmp = a[tid - 1];
		__syncthreads();
		a[tid] = tmp;
	}
	
}

void set(int *a){
	for(int i=0;i<N;i++)
		a[i] = rand() % 20;
}
void display(int *a){
	for(int i=0;i<N;i++)
		printf("%d\t", a[i]);
	printf("\n");
}
int main() {
	// 1- declare all variables
	int *a;
	int *dev_a;
	//determine the number of bytes
	int numBytes = N * sizeof(int);
	// 2- allocate host memory
	a = (int*)malloc(numBytes);
	// 3- allocate device memory
	cudaMalloc((void**)&dev_a, numBytes);
	// initialize host vector a
	set(a);
	display(a);
	// 4- transfer data from host memory to device memory
	cudaMemcpy(dev_a, a, numBytes,cudaMemcpyHostToDevice);
	// 5- launch the kernel
	shiftRight<<<1,T>>>(dev_a);
	// 6- transfer result back to host memory from device memory
	cudaMemcpy(a, dev_a, numBytes,cudaMemcpyDeviceToHost);
	// display the results
	display(a);
	
	// 7- deallocate host and device memory
	free(a); 
	cudaFree(dev_a); 
	return 0;
}
