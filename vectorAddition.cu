#include<stdio.h>
#define N 10  // vector size
#define T 10 // max threads per block
__global__ void vecAdd (int *a, int *b, int *c){
	int tid = threadIdx.x + blockDim.x*blockIdx.x;
	if(tid<N)
		c[tid] = a[tid] + b[tid];
}
void set(int *a){
	for(int i=0;i<N;i++)
		a[i] = rand() % 20;
}
void display(int *a){
	for(int i=0;i<N;i++)
		printf("%d\t", a[i]);
	printf("\n");
}
int main() {
	// 1- declare all variables
	int *a, *b, *c;
	int *dev_a, *dev_b, *dev_c;
	//determine the number of bytes
	int numBytes = N * sizeof(int);
	// 2- allocate host memory
	a = (int*)malloc(numBytes);
	b = (int*)malloc(numBytes);
	c = (int*)malloc(numBytes);
	// 3- allocate device memory
	cudaMalloc((void**)&dev_a, numBytes);
	cudaMalloc((void**)&dev_b, numBytes);
	cudaMalloc((void**)&dev_c, numBytes);
	// initialize host vector a
	set(a);
	set(b);
	// 4- transfer data from host memory to device memory
	cudaMemcpy(dev_a, a, numBytes,cudaMemcpyHostToDevice);
	cudaMemcpy(dev_b, b, numBytes,cudaMemcpyHostToDevice);
	// 5- launch the kernel
	vecAdd<<<1,T>>>(dev_a,dev_b,dev_c);
	// 6- transfer result back to host memory from device memory
	cudaMemcpy(c, dev_c, numBytes,cudaMemcpyDeviceToHost);
	// display the results
	display(a);
	display(b);
	display(c);
	// 7- deallocate host and device memory
	free(a); free(b); free(c);
	cudaFree(dev_a); cudaFree(dev_b); cudaFree(dev_c);	
	return 0;
}
