CUDA Tutorial

Compile and Run

$ nvcc  helloworld.cu -arch compute_20 -o hello
$ ./hello

$ nvcc vectorAddition.cu -o vectorAddition
$ ./vectorAddition

$ nvcc matrixAddition-1D-grid-2D-blocks.cu -o matrixAddition-1D-grid-2D-blocks
$ ./matrixAddition-1D-grid-2D-blocks

$ nvcc matrixAddition-2D-grid-2D-blocks.cu -o matrixAddition-2D-grid-2D-blocks
$ ./matrixAddition-2D-grid-2D-blocks
